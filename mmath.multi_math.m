% Copyright (C) 2018-2022 AlaskanEmily
%
% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at http://mozilla.org/MPL/2.0/.

:- module mmath.multi_math.

%==============================================================================%
:- interface.
%==============================================================================%

:- import_module float.

% add(A, B, C) :- C = A + B
% Used for multi-moded addition.
:- pred add(float, float, float).
:- mode add(in, in, uo) is det.
:- mode add(in, uo, in) is det.
:- mode add(uo, in, in) is det.

%------------------------------------------------------------------------------%

% sub(A, B, C) :- C = A - B
% Used for multi-moded addition.
:- pred sub(float, float, float).
:- mode sub(in, in, uo) is det.
:- mode sub(in, uo, in) is det.
:- mode sub(uo, in, in) is det.

%------------------------------------------------------------------------------%

% mul(A, B, C) :- C = A * B
% Used for multi-moded multiplication
:- pred mul(float, float, float).
:- mode mul(in, in, uo) is det.
:- mode mul(in, uo, in) is det.
:- mode mul(uo, in, in) is det.

%------------------------------------------------------------------------------%

% div(A, B, C) :- C = A / B
% Used for multi-moded division
% Throws a math.domain_error exception if A or B are zero
:- pred div(float, float, float).
:- mode div(in, in, uo) is det.
:- mode div(in, uo, in) is det.
:- mode div(uo, in, in) is det.

%==============================================================================%
:- implementation.
%==============================================================================%

:- pragma promise_pure(add/3).
:- pragma promise_pure(sub/3).
:- pragma promise_pure(mul/3).
:- pragma promise_pure(div/3).

%------------------------------------------------------------------------------%

% C = A + B
add(A::in, B::in, C::uo) :- C = A + B.
add(A::in, B::uo, C::in) :- C - A = B.
add(A::uo, B::in, C::in) :- C - B = A.

%------------------------------------------------------------------------------%

% C = A - B
sub(A::in, B::in, C::uo) :- C = A - B.
sub(A::in, B::uo, C::in) :- A - C = B.
sub(A::uo, B::in, C::in) :- C + B = A.

%------------------------------------------------------------------------------%

% C = A * B
mul(A::in, B::in, C::uo) :- C = A * B.
mul(A::in, B::uo, C::in) :- A / C = B.
mul(A::uo, B::in, C::in) :- B / C = A.

%------------------------------------------------------------------------------%

% C = A / B
div(A::in, B::in, C::uo) :- C = A / B.
div(A::in, B::uo, C::in) :- A / C = B.
div(A::uo, B::in, C::in) :- B * C = A.
