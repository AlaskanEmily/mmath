% Copyright (C) 2018-2022 AlaskanEmily
%
% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at http://mozilla.org/MPL/2.0/.

:- module mmath.geometry2d.rtree.

%==============================================================================%
% Implements rtree typeclass for the geometry2d.rectangle type
:- interface.
%==============================================================================%

:- use_module rtree.
:- import_module list.

%------------------------------------------------------------------------------%

:- instance rtree.region(rectangle).

%------------------------------------------------------------------------------%

:- type geometry2d.rtree.rtree(T) == rtree.rtree(rectangle, T).

%------------------------------------------------------------------------------%

:- pred find(rtree.rtree(rectangle, T), point, T).
:- mode find(in, in, out) is semidet.

%------------------------------------------------------------------------------%

:- pred find_all(rtree.rtree(rectangle, T), point, list(T)).
:- mode find_all(in, in, out) is semidet.

%==============================================================================%
:- implementation.
%==============================================================================%

:- import_module float.

%------------------------------------------------------------------------------%

find(Tree, Point, Out) :- find_all(Tree, Point, [Out|_]).

%------------------------------------------------------------------------------%

find_all(Tree, point(X, Y),
    rtree.search_contains(Tree, rectangle(X, Y, 1.0, 1.0))).

%------------------------------------------------------------------------------%

:- instance rtree.region(rectangle) where [
    (intersects(R1, R2) :- rectangles_intersect(R1, R2)),
    rtree.bounding_region(R1, R2) = rectangle_union(R1, R2),
    (contains(R1, R2) :- rectangle_contains(R1, R2)),
    rtree.size(rectangle(_, _, W, H)) = (W * H),
    (rtree.bounding_region_size(
        rectangle(X1, Y1, W1, H1),
        rectangle(X2, Y2, W2, H2)) = Size :-
        Left   = min(X1, X2),
        Top    = min(Y1, Y2),
        Right  = max(X1 + W1, X2 + W2),
        Bottom = max(Y1 + H1, Y2 + H2),
        Size = (Right - Left) * (Bottom - Top))
].
