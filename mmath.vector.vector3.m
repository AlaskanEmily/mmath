% Copyright (C) 2017-2022 AlaskanEmily
%
% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at http://mozilla.org/MPL/2.0/.

:- module mmath.vector.vector3.

%==============================================================================%
% 3D vector implementation.
:- interface.
%==============================================================================%

:- type vector ---> vector(x::float, y::float, z::float).

%------------------------------------------------------------------------------%

:- instance vector(vector3).

%------------------------------------------------------------------------------%

:- func get_x(vector3) = float.
:- func get_y(vector3) = float.
:- func get_z(vector3) = float.

%------------------------------------------------------------------------------%

:- func (vector3::in) + (vector3::in) = (vector3::uo) is det.
:- func (vector3::in) - (vector3::in) = (vector3::uo) is det.
:- func (vector3::in) * (vector3::in) = (vector3::uo) is det.
:- func (vector3::in) / (vector3::in) = (vector3::uo) is det.

%------------------------------------------------------------------------------%

:- func cross(vector3, vector3) = vector3.

%------------------------------------------------------------------------------%

:- pred foldl(pred(float, T, T), vector3, T, T).
:- mode foldl(pred(in, in, out) is det, in, in, out) is det.
:- mode foldl(pred(in, in, out) is semidet, in, in, out) is semidet.
:- mode foldl(pred(in, di, uo) is det, in, di, uo) is det.
:- mode foldl(pred(in, mdi, muo) is semidet, in, mdi, muo) is semidet.
:- mode foldl(pred(in, mdi, muo) is det, in, mdi, muo) is det.

%------------------------------------------------------------------------------%

:- pred foldr(pred(float, T, T), vector3, T, T).
:- mode foldr(pred(in, in, out) is det, in, in, out) is det.
:- mode foldr(pred(in, in, out) is semidet, in, in, out) is semidet.
:- mode foldr(pred(in, di, uo) is det, in, di, uo) is det.
:- mode foldr(pred(in, mdi, muo) is semidet, in, mdi, muo) is semidet.
:- mode foldr(pred(in, mdi, muo) is det, in, mdi, muo) is det.

%------------------------------------------------------------------------------%

:- pred foldl_field(pred(func(vector3)=(float), vector3, T, T), vector3, T, T).
:- mode foldl_field(pred(func(in)=(out) is det, in, in, out) is det, in, in, out) is det.
:- mode foldl_field(pred(func(in)=(out) is det, in, in, out) is semidet, in, in, out) is semidet.
:- mode foldl_field(pred(func(in)=(out) is det, in, mdi, muo) is semidet, in, mdi, muo) is semidet.
:- mode foldl_field(pred(func(in)=(out) is det, in, mdi, muo) is det, in, mdi, muo) is det.

%------------------------------------------------------------------------------%

:- pred foldr_field(pred(func(vector3)=(float), vector3, T, T), vector3, T, T).
:- mode foldr_field(pred(func(in)=(out) is det, in, in, out) is det, in, in, out) is det.
:- mode foldr_field(pred(func(in)=(out) is det, in, in, out) is semidet, in, in, out) is semidet.

%------------------------------------------------------------------------------%

:- pred map(pred(float, float), vector3, vector3).
:- mode map(pred(in, out) is det, in, out) is det.
:- mode map(pred(in, out) is semidet, in, out) is semidet.
:- mode map(pred(di, uo) is det, di, uo) is det.
:- mode map(pred(mdi, muo) is semidet, mdi, muo) is semidet.
:- mode map(pred(mdi, muo) is det, mdi, muo) is det.

%------------------------------------------------------------------------------%

:- pred map_field(pred(func(vector3)=(float), vector3, float), vector3, vector3).
:- mode map_field(pred(func(in)=(out) is det, in, out) is det, in, out) is det.
:- mode map_field(pred(func(in)=(out) is det, in, out) is semidet, in, out) is semidet.

%------------------------------------------------------------------------------%

:- pred map_foldl(pred(float, float, T, T), vector3, vector3, T, T).
:- mode map_foldl(pred(in, out, in, out) is det, in, out, in, out) is det.
:- mode map_foldl(pred(in, out, in, out) is semidet, in, out, in, out) is semidet.
:- mode map_foldl(pred(in, out, mdi, muo) is semidet, in, out, mdi, muo) is semidet.
:- mode map_foldl(pred(in, out, mdi, muo) is det, in, out, mdi, muo) is det.
:- mode map_foldl(pred(di, uo, in, out) is det, di, uo, in, out) is det.
:- mode map_foldl(pred(in, out, di, uo) is det, in, out, di, uo) is det.
:- mode map_foldl(pred(di, uo, di, uo) is det, di, uo, di, uo) is det.
:- mode map_foldl(pred(mdi, muo, in, out) is det, mdi, muo, in, out) is det.
:- mode map_foldl(pred(mdi, muo, in, out) is semidet, mdi, muo, in, out) is semidet.
:- mode map_foldl(pred(mdi, muo, mdi, muo) is semidet, mdi, muo, mdi, muo) is semidet.
:- mode map_foldl(pred(mdi, muo, mdi, muo) is det, mdi, muo, mdi, muo) is det.
:- mode map_foldl(pred(mdi, muo, di, uo) is det, mdi, muo, di, uo) is det.

%------------------------------------------------------------------------------%

:- pred map_foldr(pred(float, float, T, T), vector3, vector3, T, T).
:- mode map_foldr(pred(in, out, in, out) is det, in, out, in, out) is det.
:- mode map_foldr(pred(in, out, in, out) is semidet, in, out, in, out) is semidet.
:- mode map_foldr(pred(in, out, mdi, muo) is semidet, in, out, mdi, muo) is semidet.
:- mode map_foldr(pred(in, out, mdi, muo) is det, in, out, mdi, muo) is det.
:- mode map_foldr(pred(di, uo, in, out) is det, di, uo, in, out) is det.
:- mode map_foldr(pred(in, out, di, uo) is det, in, out, di, uo) is det.
:- mode map_foldr(pred(di, uo, di, uo) is det, di, uo, di, uo) is det.
:- mode map_foldr(pred(mdi, muo, in, out) is det, mdi, muo, in, out) is det.
:- mode map_foldr(pred(mdi, muo, in, out) is semidet, mdi, muo, in, out) is semidet.
:- mode map_foldr(pred(mdi, muo, mdi, muo) is semidet, mdi, muo, mdi, muo) is semidet.
:- mode map_foldr(pred(mdi, muo, mdi, muo) is det, mdi, muo, mdi, muo) is det.
:- mode map_foldr(pred(mdi, muo, di, uo) is det, mdi, muo, di, uo) is det.

%==============================================================================%
:- implementation.
%==============================================================================%

:- import_module float.

%------------------------------------------------------------------------------%

get_x(V) = V ^ x.
get_y(V) = V ^ y.
get_z(V) = V ^ z.

%------------------------------------------------------------------------------%

(vector(X1, Y1, Z1)) + (vector(X2, Y2, Z2)) = (vector(X1+X2, Y1+Y2, Z1+Z2)).
(vector(X1, Y1, Z1)) - (vector(X2, Y2, Z2)) = (vector(X1-X2, Y1-Y2, Z1-Z2)).
(vector(X1, Y1, Z1)) * (vector(X2, Y2, Z2)) = (vector(X1*X2, Y1*Y2, Z1*Z2)).
(vector(X1, Y1, Z1)) / (vector(X2, Y2, Z2)) = (vector(X1/X2, Y1/Y2, Z1/Z2)).

%------------------------------------------------------------------------------%

cross(vector(X1, Y1, Z1), vector(X2, Y2, Z2)) =
    vector((Y1*Z2) - (Z1*Y2), (Z1*X2) - (X1*Z2), (X1*Y2) - (Y1*X2)).

%------------------------------------------------------------------------------%

:- instance vector(vector3) where [
    magnitude_squared(vector(X, Y, Z)) = ((X*X) + (Y*Y) + (Z*Z)),
    scale(vector(X, Y, Z), S) = vector(X*S, Y*S, Z*S),
    multiply(vector(X1, Y1, Z1), vector(X2, Y2, Z2)) = vector(X1*X2, Y1*Y2, Z1*Z2),
    divide(vector(X1, Y1, Z1), vector(X2, Y2, Z2)) = vector(X1/X2, Y1/Y2, Z1/Z2),
    add(vector(X1, Y1, Z1), vector(X2, Y2, Z2)) = vector(X1+X2, Y1+Y2, Z1+Z2),
    sub(vector(X1, Y1, Z1), vector(X2, Y2, Z2)) = vector(X1-X2, Y1-Y2, Z1-Z2),
    dot(vector(X1, Y1, Z1), vector(X2, Y2, Z2)) = ((X1*X2) + (Y1*Y2) + (Z1*Z2)),
    reciprocal(vector(X, Y, Z)) = vector(1.0/X, 1.0/Y, 1.0/Z)
].

%------------------------------------------------------------------------------%

foldl(Pred, vector(X, Y, Z), !T) :- Pred(X, !T), Pred(Y, !T), Pred(Z, !T).

%------------------------------------------------------------------------------%

foldr(Pred, vector(X, Y, Z), !T) :- Pred(Z, !T), Pred(Y, !T), Pred(X, !T).

%------------------------------------------------------------------------------%

foldl_field(Pred, Vec, !T) :- Pred(get_x, Vec, !T), Pred(get_y, Vec, !T), Pred(get_z, Vec, !T).

%------------------------------------------------------------------------------%

foldr_field(Pred, Vec, !T) :- Pred(get_z, Vec, !T), Pred(get_y, Vec, !T), Pred(get_x, Vec, !T).

%------------------------------------------------------------------------------%

map(Pred, vector(XIn, YIn, ZIn), vector(XOut, YOut, ZOut)) :-
    Pred(XIn, XOut),
    Pred(YIn, YOut),
    Pred(ZIn, ZOut).

%------------------------------------------------------------------------------%

map_field(Pred, Vec, vector(X, Y, Z)) :-
    Pred(get_x, Vec, X),
    Pred(get_y, Vec, Y),
    Pred(get_z, Vec, Z).

%------------------------------------------------------------------------------%

map_foldl(Pred, vector(XIn, YIn, ZIn), vector(XOut, YOut, ZOut), !T) :-
    Pred(XIn, XOut, !T),
    Pred(YIn, YOut, !T),
    Pred(ZIn, ZOut, !T).

%------------------------------------------------------------------------------%

map_foldr(Pred, vector(XIn, YIn, ZIn), vector(XOut, YOut, ZOut), !T) :-
    Pred(ZIn, ZOut, !T),
    Pred(YIn, YOut, !T),
    Pred(XIn, XOut, !T).

%------------------------------------------------------------------------------%
