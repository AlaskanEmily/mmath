% Copyright (C) 2017-2022 AlaskanEmily
%
% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at http://mozilla.org/MPL/2.0/.

:- module mmath.geometry.segment.

%==============================================================================%
% Geometric operations related to a segment. This is parametric on the vector
% implementing the dimensions.
:- interface.
%==============================================================================%

:- import_module pair.

:- import_module mmath.vector.

%------------------------------------------------------------------------------%

:- type segment(V) ---> segment(p1::V, p2::V).

%------------------------------------------------------------------------------%

:- func length(segment(V)) = float <= vector.vector(V).

%------------------------------------------------------------------------------%

:- func translate(segment(V), V) = segment(V) <= vector.vector(V).

%------------------------------------------------------------------------------%

:- pred as_pair(segment(V), pair(V, V)).
:- mode as_pair(in, out) is det.
:- mode as_pair(out, in) is det.
:- mode as_pair(in, in) is semidet.

%------------------------------------------------------------------------------%

:- pred as_tuple(segment(V), {V, V}).
:- mode as_tuple(in, out) is det.
:- mode as_tuple(out, in) is det.
:- mode as_tuple(in, in) is semidet.

%------------------------------------------------------------------------------%

:- type side ---> left ; right ; colinear.

%------------------------------------------------------------------------------%

:- func side(segment(vector2), vector2) = side.

%------------------------------------------------------------------------------%

:- func solve(segment(V), float) = V <= vector.vector(V).

%------------------------------------------------------------------------------%

:- func project(V, segment(V)) = V <= vector.vector(V).

%------------------------------------------------------------------------------%

:- pragma type_spec(length/1, V = vector.vector2).
:- pragma type_spec(length/1, V = vector.vector3).

%------------------------------------------------------------------------------%

:- pragma type_spec(translate/2, V = vector.vector2).
:- pragma type_spec(translate/2, V = vector.vector3).

%------------------------------------------------------------------------------%

:- pragma type_spec(solve/2, V = vector.vector2).
:- pragma type_spec(solve/2, V = vector.vector3).

%------------------------------------------------------------------------------%

:- pragma type_spec(project/2, V = vector.vector2).
:- pragma type_spec(project/2, V = vector.vector3).

%==============================================================================%
:- implementation.
%==============================================================================%

:- import_module float.

:- import_module mmath.vector.vector2.

%------------------------------------------------------------------------------%

length(S) = magnitude(sub(S ^ p1, S ^ p2)).

%------------------------------------------------------------------------------%

translate(S, V) = segment(add(S ^ p1, V), add(S ^ p2, V)).

%------------------------------------------------------------------------------%

as_pair(S, (S ^ p1 - S ^ p2)).

%------------------------------------------------------------------------------%

as_tuple(S, {S ^ p1,  S ^ p2}).

%------------------------------------------------------------------------------%
% TODO!

side(segment(A, B), C) = Side :-
    Cross = vector.vector2.cross(B - A, C - A),
    ( if
        Cross > float.epsilon
    then
        Side = left
    else if
        Cross < -float.epsilon
    then
        Side = right
    else
        Side = colinear
    ).

%------------------------------------------------------------------------------%

solve(S, T) = Out :-
    ( if
        T = 0.0
    then
        Out = S ^ p1
    else if
        T = 1.0
    then
        Out = S ^ p2
    else if
        T = 0.5
    then
        Out = midpoint(add(S ^ p1, S ^ p2))
    else
        Out = add(scale(S ^ p1, 1.0 - T), scale(S ^ p2, T))
    ).

%------------------------------------------------------------------------------%

project(V, S) = solve(S, dot(sub(S ^ p2, S ^ p1), sub(V, S ^ p1))).
