% Copyright (C) 2017-2022 AlaskanEmily
%
% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at http://mozilla.org/MPL/2.0/.

:- module mmath.matrix.

%=============================================================================%
:- interface.
%=============================================================================%

:- use_module mmath.vector.

:- include_module mmath.matrix.invert.
:- use_module mmath.matrix.invert.

%-----------------------------------------------------------------------------%

:- type matrix ---> matrix(
    a::mmath.vector.vector4,
    b::mmath.vector.vector4,
    c::mmath.vector.vector4,
    d::mmath.vector.vector4).

%-----------------------------------------------------------------------------%
% frustum(Left, Right, Top, Bottom, Near, Far) = Frustum.
:- func frustum(float, float, float, float, float, float) = matrix.

%-----------------------------------------------------------------------------%
% ortho(Left, Right, Top, Bottom, Near, Far) = Ortho.
:- func ortho(float, float, float, float, float, float) = matrix.

%-----------------------------------------------------------------------------%

:- func translate(float, float, float) = matrix.
:- mode translate(in, in, in) = (out) is det.
:- mode translate(di, di, di) = (uo) is det.
:- mode translate(mdi, mdi, mdi) = (muo) is det.

%-----------------------------------------------------------------------------%

:- func translate(mmath.vector.vector3) = matrix.
:- mode translate(in) = (out) is det.
:- mode translate(di) = (uo) is det.
:- mode translate(mdi) = (muo) is det.

%-----------------------------------------------------------------------------%

:- func scale(float, float, float) = matrix.
:- mode scale(in, in, in) = (out) is det.
:- mode scale(di, di, di) = (uo) is det.
:- mode scale(mdi, mdi, mdi) = (muo) is det.

%-----------------------------------------------------------------------------%

:- func scale(mmath.vector.vector3) = matrix.
:- mode scale(in) = (out) is det.
:- mode scale(di) = (uo) is det.
:- mode scale(mdi) = (muo) is det.

%-----------------------------------------------------------------------------%

:- func rotate(float, mmath.vector.vector3) = matrix.
:- mode rotate(in, in) = (uo) is det.
:- mode rotate(di, di) = (uo) is det.
:- mode rotate(mdi, mdi) = (muo) is det.

:- func rotate_old(float::in, mmath.vector.vector3::in) = (matrix::uo) is det.

%-----------------------------------------------------------------------------%

:- func identity = matrix.

%-----------------------------------------------------------------------------%

:- func transpose(matrix) = matrix.
:- mode transpose(in) = (out) is det.
:- mode transpose(di) = (uo) is det.
:- mode transpose(mdi) = (muo) is det.

%-----------------------------------------------------------------------------%

:- pred transpose(matrix, matrix).
:- mode transpose(in, out) is det.
:- mode transpose(out, in) is det.
:- mode transpose(in, in) is semidet.
:- mode transpose(di, uo) is det.
:- mode transpose(uo, di) is det.
:- mode transpose(mdi, muo) is det.
:- mode transpose(muo, mdi) is det.

%-----------------------------------------------------------------------------%

:- func (matrix::in) * (mmath.vector.vector4::in) = (mmath.vector.vector4::uo) is det.

%-----------------------------------------------------------------------------%

:- func transform(matrix::in, mmath.vector.vector4::in) = (mmath.vector.vector4::uo) is det.

%-----------------------------------------------------------------------------%

:- func multiply(matrix, matrix) = matrix.
:- mode multiply(in, in) = (uo) is det.
:- mode multiply(di, in) = (uo) is det.
:- mode multiply(in, di) = (uo) is det.
:- mode multiply(di, di) = (uo) is det.

:- pred multiply(matrix, matrix, matrix).
:- mode multiply(in, in, uo) is det.
:- mode multiply(di, in, uo) is det.
:- mode multiply(in, di, uo) is det.
:- mode multiply(di, di, uo) is det.
:- mode multiply(in, in, in) is semidet. % Implied.

% multiply2(A, B, C) <=> (B * A = C) <=> multiple(B, A, C)
% Useful for state-variable matrix transformations.
:- pred multiply2(matrix, matrix, matrix).
:- mode multiply2(in, in, uo) is det.
:- mode multiply2(in, di, uo) is det.
:- mode multiply2(di, di, uo) is det.

%-----------------------------------------------------------------------------%

:- func column_a(matrix::in) = (mmath.vector.vector4::uo) is det.
:- func column_b(matrix::in) = (mmath.vector.vector4::uo) is det.
:- func column_c(matrix::in) = (mmath.vector.vector4::uo) is det.
:- func column_d(matrix::in) = (mmath.vector.vector4::uo) is det.

%-----------------------------------------------------------------------------%

:- pred destructure_matrix(matrix,
    float, float, float, float,
    float, float, float, float,
    float, float, float, float,
    float, float, float, float).

:- mode destructure_matrix(in,
    out, out, out, out,
    out, out, out, out,
    out, out, out, out,
    out, out, out, out) is det.

%=============================================================================%
:- implementation.
%=============================================================================%

:- use_module exception.
:- import_module float.
:- import_module int.
:- use_module math.

:- import_module mmath.vector.vector3.
:- import_module mmath.vector.vector4.

%-----------------------------------------------------------------------------%

frustum(Left, Right, Top, Bottom, Near, Far) = matrix(
    vector(2.0 * Near / (Right - Left), 0.0, A, 0.0),
    vector(0.0, 2.0 * Near / (Top - Bottom), B, 0.0),
    vector(0.0, 0.0, C, D),
    vector(0.0, 0.0, -1.0, 0.0)) :-
    A = (Right + Left) / (Right - Left),
    B = (Top + Bottom) / (Top - Bottom),
    C = -(Far + Near) / (Far - Near),
    D = -2.0 * Far * Near / (Far - Near).

%-----------------------------------------------------------------------------%

ortho(Left, Right, Top, Bottom, Near, Far) = matrix(
    vector(2.0 / (Right - Left), 0.0, 0.0, A),
    vector(0.0, 2.0 / (Top - Bottom), 0.0, B),
    vector(0.0, 0.0, -2.0 / (Far - Near), C),
    vector(0.0, 0.0, 0.0, 1.0)) :-
    A = (Right + Left) / (Right - Left),
    B = (Top + Bottom) / (Top - Bottom),
    C = -(Far + Near) / (Far - Near).

%-----------------------------------------------------------------------------%

translate(X, Y, Z) = matrix(
    vector(1.0, 0.0, 0.0, X),
    vector(0.0, 1.0, 0.0, Y),
    vector(0.0, 0.0, 1.0, Z),
    vector(0.0, 0.0, 0.0, 1.0)).

%-----------------------------------------------------------------------------%

translate(vector(X, Y, Z)) = translate(X, Y, Z).

%-----------------------------------------------------------------------------%

scale(X, Y, Z) = matrix(
    vector(X,   0.0, 0.0, 0.0),
    vector(0.0, Y,   0.0, 0.0),
    vector(0.0, 0.0, Z,   0.0),
    vector(0.0, 0.0, 0.0, 1.0)).

%-----------------------------------------------------------------------------%

scale(vector(X, Y, Z)) = scale(X, Y, Z).

%-----------------------------------------------------------------------------%

rotate_old(A, Vec) = M :- transpose(rotate(A, Vec), M).

%-----------------------------------------------------------------------------%

rotate(A, Vec) = matrix(
    vector(X*X*(1.0-C)+C,   X*Y*(1.0-C)-Z*S, X*Z*(1.0-C)+Y*S, 0.0),
    vector(Y*X*(1.0-C)+Z*S, Y*Y*(1.0-C)+C,   Y*Z*(1.0-C)-X*S, 0.0),
    vector(Z*X*(1.0-C)-Y*S, Z*Y*(1.0-C)+X*S, Z*Z*(1.0-C)+C,   0.0),
    vector(0.0, 0.0, 0.0, 1.0)) :-
    vector(X, Y, Z) = mmath.vector.normalize(Vec),
    S = math.sin(A),
    C = math.cos(A).

%-----------------------------------------------------------------------------%

identity = matrix(
    vector(1.0, 0.0, 0.0, 0.0),
    vector(0.0, 1.0, 0.0, 0.0),
    vector(0.0, 0.0, 1.0, 0.0),
    vector(0.0, 0.0, 0.0, 1.0)).

%-----------------------------------------------------------------------------%

(matrix(V1, V2, V3, V4)) * (V) =
    vector(mmath.vector.dot(V, V1), mmath.vector.dot(V, V2), mmath.vector.dot(V, V3), mmath.vector.dot(V, V4)).

%-----------------------------------------------------------------------------%

transform(M, V) = (M * V).

%-----------------------------------------------------------------------------%

transpose(M1) = M2 :- transpose(M1, M2).

%-----------------------------------------------------------------------------%

transpose(
    matrix(
        vector(A, B, C, D),
        vector(E, F, G, H),
        vector(I, J, K, L),
        vector(M, N, O, P)),
    matrix(
        vector(A, E, I, M),
        vector(B, F, J, N),
        vector(C, G, K, O),
        vector(D, H, L, P))).

%-----------------------------------------------------------------------------%

multiply(matrix(A, B, C, D), matrix(AIn, BIn, CIn, DIn)) = matrix(
    vector(mmath.vector.dot(A, E), mmath.vector.dot(A, F), mmath.vector.dot(A, G), mmath.vector.dot(A, H)),
    vector(mmath.vector.dot(B, E), mmath.vector.dot(B, F), mmath.vector.dot(B, G), mmath.vector.dot(B, H)),
    vector(mmath.vector.dot(C, E), mmath.vector.dot(C, F), mmath.vector.dot(C, G), mmath.vector.dot(C, H)),
    vector(mmath.vector.dot(D, E), mmath.vector.dot(D, F), mmath.vector.dot(D, G), mmath.vector.dot(D, H))) :-
    E = vector(AIn ^ x, BIn ^ x, CIn ^ x, DIn ^ x),
    F = vector(AIn ^ y, BIn ^ y, CIn ^ y, DIn ^ y),
    G = vector(AIn ^ z, BIn ^ z, CIn ^ z, DIn ^ z),
    H = vector(AIn ^ w, BIn ^ w, CIn ^ w, DIn ^ w).

multiply(M1, M2, multiply(M1, M2)).

multiply2(M2, M1, multiply(M1, M2)).

%-----------------------------------------------------------------------------%

column_a(M) = vector(M ^ a ^ x + 0.0, M ^ b ^ x + 0.0, M ^ c ^ x + 0.0, M ^ d ^ x + 0.0).
column_b(M) = vector(M ^ a ^ y + 0.0, M ^ b ^ y + 0.0, M ^ c ^ y + 0.0, M ^ d ^ y + 0.0).
column_c(M) = vector(M ^ a ^ z + 0.0, M ^ b ^ z + 0.0, M ^ c ^ z + 0.0, M ^ d ^ z + 0.0).
column_d(M) = vector(M ^ a ^ w + 0.0, M ^ b ^ w + 0.0, M ^ c ^ w + 0.0, M ^ d ^ w + 0.0).

%-----------------------------------------------------------------------------%

destructure_matrix(
    matrix(
        vector(V0,  V1,  V2,  V3),
        vector(V4,  V5,  V6,  V7),
        vector(V8,  V9,  V10, V11),
        vector(V12, V13, V14, V15)),
    V0,  V1,  V2,  V3,
    V4,  V5,  V6,  V7,
    V8,  V9,  V10, V11,
    V12, V13, V14, V15).

