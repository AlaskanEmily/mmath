% Copyright (C) 2017-2022 AlaskanEmily
%
% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at http://mozilla.org/MPL/2.0/.

:- module mmath.vector.

%==============================================================================%
:- interface.
%==============================================================================%

:- include_module mmath.vector.vector2.
:- include_module mmath.vector.vector3.
:- include_module mmath.vector.vector4.

:- use_module mmath.vector.vector2.
:- use_module mmath.vector.vector3.
:- use_module mmath.vector.vector4.

%------------------------------------------------------------------------------%

:- type vector2 == mmath.vector.vector2.vector.
:- type vector3 == mmath.vector.vector3.vector.
:- type vector4 == mmath.vector.vector4.vector.

%------------------------------------------------------------------------------%

:- func vector3_to_vector2(vector3) = vector2.
:- func vector4_to_vector2(vector4) = vector2.
:- func vector4_to_vector3(vector4) = vector3.
:- func vector2_to_vector3(vector2, float) = vector3.
:- func vector2_to_vector4(vector2, float, float) = vector4.
:- func vector3_to_vector4(vector3, float) = vector4.

%------------------------------------------------------------------------------%

:- typeclass vector(Vec) where [
    func magnitude_squared(Vec::in) = (float::uo) is det,
    func scale(Vec::in, float::in) = (Vec::uo) is det,
    func multiply(Vec::in, Vec::in) = (Vec::uo) is det,
    func divide(Vec::in, Vec::in) = (Vec::uo) is det,
    func add(Vec::in, Vec::in) = (Vec::uo) is det,
    func sub(Vec::in, Vec::in) = (Vec::uo) is det,
    func dot(Vec::in, Vec::in) = (float::uo) is det,
    func reciprocal(Vec::in) = (Vec::uo) is det
].

%------------------------------------------------------------------------------%

:- pred near(float, Vec, Vec) <= vector(Vec).
:- mode near(in, in, in) is semidet.

%------------------------------------------------------------------------------%

:- func normalize(Vec::in) = (Vec::uo) is det <= vector(Vec).

%------------------------------------------------------------------------------%

:- func midpoint(Vec::in) = (Vec::uo) is det <= vector(Vec).

%------------------------------------------------------------------------------%

:- func negate(Vec::in) = (Vec::uo) is det <= vector(Vec).

%------------------------------------------------------------------------------%

:- func magnitude(Vec::in) = (float::uo) is det <= vector(Vec).

%------------------------------------------------------------------------------%

:- func (Vec::in) * (float::in) = (Vec::uo) is det <= vector(Vec).

%------------------------------------------------------------------------------%

:- func (Vec::in) / (float::in) = (Vec::uo) is det <= vector(Vec).

%------------------------------------------------------------------------------%

:- func - (Vec::in) = (Vec::uo) is det <= vector(Vec).

%==============================================================================%
:- implementation.
%==============================================================================%

:- use_module math.
:- import_module float.

%------------------------------------------------------------------------------%

vector3_to_vector2(vector.vector3.vector(X, Y, _Z)) = vector.vector2.vector(X, Y).
vector4_to_vector2(vector.vector4.vector(X, Y, _Z, _W)) = vector.vector2.vector(X, Y).
vector4_to_vector3(vector.vector4.vector(X, Y, Z, _W)) = vector.vector3.vector(X, Y, Z).

%------------------------------------------------------------------------------%

vector2_to_vector3(vector.vector2.vector(X, Y), Z) = vector.vector3.vector(X, Y, Z).
vector2_to_vector4(vector.vector2.vector(X, Y), Z, W) = vector.vector4.vector(X, Y, Z, W).
vector3_to_vector4(vector.vector3.vector(X, Y, Z), W) = vector.vector4.vector(X, Y, Z, W).

%------------------------------------------------------------------------------%

near(Epsilon, V1, V2) :- magnitude_squared(sub(V1, V2)) =< (Epsilon * Epsilon).

%------------------------------------------------------------------------------%

normalize(Vec) = scale(Vec, 1.0 / magnitude(Vec)).

%------------------------------------------------------------------------------%

midpoint(Vec) = scale(Vec, 0.5).

%------------------------------------------------------------------------------%

negate(Vec) = scale(Vec, -1.0).

%------------------------------------------------------------------------------%

magnitude(Vec) = math.sqrt(magnitude_squared(Vec)) + 0.0.

%------------------------------------------------------------------------------%

(Vec) * (F) = scale(Vec, F).

%------------------------------------------------------------------------------%

(Vec) / (F) = scale(Vec, 1.0/F).

%------------------------------------------------------------------------------%

- (Vec) = negate(Vec).
