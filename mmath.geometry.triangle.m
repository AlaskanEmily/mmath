% Copyright (C) 2017-2022 AlaskanEmily
%
% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at http://mozilla.org/MPL/2.0/.

:- module mmath.geometry.triangle.

%==============================================================================%
% Geometric operations related to a triangle. This is parametric on the vector
% implementing the dimensions.
:- interface.
%==============================================================================%

:- import_module mmath.vector.

%------------------------------------------------------------------------------%

:- type triangle(V) ---> triangle(p1::V, p2::V, p3::V).

%------------------------------------------------------------------------------%

:- func normal(triangle(vector3)) = vector3.

%------------------------------------------------------------------------------%

:- func plane_d(triangle(vector3), vector3) = float.

%------------------------------------------------------------------------------%

:- pred collinear(triangle(V), V, float, float, float) <= vector(V).
:- mode collinear(in, in, out, out, out) is det.

%------------------------------------------------------------------------------%

:- pragma type_spec(collinear/5, V = mmath.vector.vector2).
:- pragma type_spec(collinear/5, V = mmath.vector.vector3).

%==============================================================================%
:- implementation.
%==============================================================================%

:- import_module float.

%------------------------------------------------------------------------------%

normal(Tri) = normalize(cross(Tri ^ p2 - Tri ^ p1, Tri ^ p3 - Tri ^ p1)).

%------------------------------------------------------------------------------%

plane_d(T, Normal) = dot(T ^ p1, Normal).

%------------------------------------------------------------------------------%

collinear(Tri, V,
    dot(P1_P2, P1_V) / magnitude_squared(P1_P2),
    dot(P2_P3, P2_V) / magnitude_squared(P2_P3),
    dot(P3_P1, P3_V) / magnitude_squared(P3_P1)) :-
    
    P1_P2 = sub(Tri ^ p2, Tri ^ p1),
    P2_P3 = sub(Tri ^ p3, Tri ^ p2),
    P3_P1 = sub(Tri ^ p1, Tri ^ p3),
    P1_V = sub(V, Tri ^ p1),
    P2_V = sub(V, Tri ^ p2),
    P3_V = sub(V, Tri ^ p3).
