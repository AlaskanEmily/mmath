% Copyright (C) 2017-2022 AlaskanEmily
%
% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at http://mozilla.org/MPL/2.0/.

:- module mmath.geometry2d.

%==============================================================================%
:- interface.
%==============================================================================%

:- import_module list.

:- import_module mmath.vector.

%------------------------------------------------------------------------------%

:- include_module mmath.geometry2d.rtree.

%------------------------------------------------------------------------------%

:- type segment ---> segment(seg_p1::vector2, seg_p2::vector2).

:- func segment(float, float, float, float) = segment.
:- mode segment(in, in, in, in) = (out) is det.
:- mode segment(di, di, di, di) = (uo) is det.
:- mode segment(mdi, mdi, mdi, mdi) = (muo) is det.
:- mode segment(out, out, out, out) = (in) is det.
:- mode segment(uo, uo, uo, uo) = (di) is det.
:- mode segment(muo, muo, muo, muo) = (mdi) is det.
:- mode segment(in, in, in, in) = (in) is semidet. % implied.

:- func seg_x1(segment) = float.
:- func seg_y1(segment) = float.
:- func seg_x2(segment) = float.
:- func seg_y2(segment) = float.

:- func 'seg_x1 :='(float, segment) = segment.
:- func 'seg_y1 :='(float, segment) = segment.
:- func 'seg_x2 :='(float, segment) = segment.
:- func 'seg_y2 :='(float, segment) = segment.

%------------------------------------------------------------------------------%

:- type rectangle ---> rectangle(
    rect_pt::vector2,
    rect_w::float,
    rect_h::float).

:- func rectangle(float, float, float, float) = rectangle.
:- mode rectangle(in, in, in, in) = (out) is det.
:- mode rectangle(di, di, di, di) = (uo) is det.
:- mode rectangle(mdi, mdi, mdi, mdi) = (muo) is det.
:- mode rectangle(out, out, out, out) = (in) is det.
:- mode rectangle(uo, uo, uo, uo) = (di) is det.
:- mode rectangle(muo, muo, muo, muo) = (mdi) is det.
:- mode rectangle(in, in, in, in) = (in) is semidet. % implied.

:- func rect_x(rectangle) = float.
:- func rect_y(rectangle) = float.

:- func 'rect_x :='(float, rectangle) = rectangle.
:- func 'rect_y :='(float, rectangle) = rectangle.

%------------------------------------------------------------------------------%

:- type circle ---> circle(circle_center::vector2, radius::float).

:- func circle(float, float, float) = circle.
:- mode circle(in, in, in) = (out) is det.
:- mode circle(di, di, di) = (uo) is det.
:- mode circle(mdi, mdi, mdi) = (muo) is det.
:- mode circle(out, out, out) = (in) is det.
:- mode circle(uo, uo, uo) = (di) is det.
:- mode circle(muo, muo, muo) = (mdi) is det.
:- mode circle(in, in, in) = (in) is semidet. % implied.

:- func circle_x(circle) = float.
:- func circle_y(circle) = float.

:- func 'circle_x :='(float, circle) = circle.
:- func 'circle_y :='(float, circle) = circle.

%------------------------------------------------------------------------------%

:- type point == vector2.

:- func point(float, float) = point.
:- mode point(in, in) = (out) is det.
:- mode point(di, di) = (uo) is det.
:- mode point(mdi, mdi) = (muo) is det.
:- mode point(out, out) = (in) is det.
:- mode point(uo, uo) = (di) is det.
:- mode point(muo, muo) = (mdi) is det.
:- mode point(in, in) = (in) is semidet. % implied.

:- func x(point) = float.
:- func y(point) = float.

:- func 'x :='(float, point) = point.
:- func 'y :='(float, point) = point.

%------------------------------------------------------------------------------%

:- type triangle ---> triangle(
    tri_p1::vector2,
    tri_p2::vector2,
    tri_p3::vector2).

:- func triangle(float, float, float, float, float, float) = triangle.
:- mode triangle(in, in, in, in, in, in) = (out) is det.
:- mode triangle(di, di, di, di, di, di) = (uo) is det.
:- mode triangle(mdi, mdi, mdi, mdi, mdi, mdi) = (muo) is det.
:- mode triangle(out, out, out, out, out, out) = (in) is det.
:- mode triangle(uo, uo, uo, uo, uo, uo) = (di) is det.
:- mode triangle(muo, muo, muo, muo, muo, muo) = (mdi) is det.
:- mode triangle(in, in, in, in, in, in) = (in) is semidet. % implied.

:- func tri_x1(triangle) = float.
:- func tri_y1(triangle) = float.
:- func tri_x2(triangle) = float.
:- func tri_y2(triangle) = float.
:- func tri_x3(triangle) = float.
:- func tri_y3(triangle) = float.

:- func 'tri_x1 :='(float, triangle) = triangle.
:- func 'tri_y1 :='(float, triangle) = triangle.
:- func 'tri_x2 :='(float, triangle) = triangle.
:- func 'tri_y2 :='(float, triangle) = triangle.
:- func 'tri_x3 :='(float, triangle) = triangle.
:- func 'tri_y3 :='(float, triangle) = triangle.

%------------------------------------------------------------------------------%

:- func length(segment) = float.

%------------------------------------------------------------------------------%

:- func distance(point, point) = float.

%------------------------------------------------------------------------------%

:- func distance(float, float, float, float) = float.

%------------------------------------------------------------------------------%

:- func grow_rectangle(float, rectangle) = rectangle.
:- pred grow_rectangle(float, rectangle, rectangle).
:- mode grow_rectangle(in, in, uo) is det.

%------------------------------------------------------------------------------%

:- func grow_rectangle(float, float, rectangle) = rectangle.
:- pred grow_rectangle(float, float, rectangle, rectangle).
:- mode grow_rectangle(in, in, in, uo) is det.

%------------------------------------------------------------------------------%

% Promise equivalence of the two version of grow_rectangle
%:- promise all[Size, Rectangle] (
%    grow_rectangle(Size, Rectangle, Grown)
%<=>
%    grow_rectangle(Size, Size, Rectangle, Grown)
%).

%------------------------------------------------------------------------------%

:- pred rectangle_intersection(rectangle, rectangle, rectangle).
:- mode rectangle_intersection(in, in, uo) is semidet.
:- mode rectangle_intersection(in, in, in) is semidet.

%------------------------------------------------------------------------------%

:- pred rectangles_intersect(rectangle::in, rectangle::in) is semidet.

%------------------------------------------------------------------------------%

:- pred rectangle_union(rectangle::in, rectangle::in, rectangle::uo) is det.

%------------------------------------------------------------------------------%

:- func rectangle_union(rectangle, rectangle) = rectangle.

%------------------------------------------------------------------------------%

:- pred rectangle_contains(rectangle::in, rectangle::in) is semidet.

%------------------------------------------------------------------------------%

:- func point_to_vector(geometry2d.point) = vector.vector2.

%------------------------------------------------------------------------------%

% Promise the associativity of rectangle intersections
%:- promise all[Rectangle1, Rectangle2] (
%    some [RectangleIntersection]
%    rectangle_intersection(Rectangle1, Rectangle2, RectangleIntersection)
%<=>
%    rectangle_intersection(Rectangle2, Rectangle1, RectangleIntersection)
%).

%------------------------------------------------------------------------------%

% Plox not crash
%:- promise all[Rectangle] (
%    rectangle_intersection(Rectangle, Rectangle, Rectangle)
%).

%------------------------------------------------------------------------------%

:- pred triangle_segments(triangle, segment, segment, segment).
:- mode triangle_segments(in, out, out, out) is det.

%------------------------------------------------------------------------------%

:- func translate(point, T) = T <= shape(T).

%------------------------------------------------------------------------------%

% intercept(Segment, X, Y)
:- func slope(segment) = float.
:- pred slope(segment, float).
:- mode slope(in, out) is det.

%------------------------------------------------------------------------------%

% intercept(Segment, X)
:- pred intercept(segment, float).
:- mode intercept(in, uo) is semidet.

% intercept(Segment, X, Slope)
:- pred intercept(segment, float, float).
:- mode intercept(in, uo, uo) is semidet.

%------------------------------------------------------------------------------%

% solve_x(Segment, X, Y).
% Solves the segment's line for Y given an X.
:- pred solve_x(segment::in, float::in, float::uo) is semidet.

%------------------------------------------------------------------------------%
% Unifies if the segment intersects.
% Point is a point which exists on both segments.
:- pred collide_segments(segment::in, segment::in, point::out) is semidet.

%------------------------------------------------------------------------------%
% Unifies if the segment intersects with the circle.
% Point is a point which exists on the segment and within the circle.
:- pred collide_segment_circle(segment::in, circle::in, point::out) is semidet.

%------------------------------------------------------------------------------%
% Unifies if the segment intersects with the circle.
% Point is a point which exists on the segment and within the circle.
:- pred collide_segment_rectangle(segment::in, rectangle::in, point::out) is semidet.

%------------------------------------------------------------------------------%

:- typeclass shape(T) where [
    %func area(T) = float,
    func center(T) = point,
    func bounding_box(T) = rectangle,
    pred point_inside(T::in, point::in) is semidet,
    pred translate(point, T, T),
    mode translate(in, in, out) is det,
    mode translate(in, out, in) is det,
    mode translate(out, in, in) is semidet,
    % Produce a list of points that represents a set of vertices which define the shape.
    % convex(Shape, PointSize, Points)
    pred convex(T::in, float::out, list(point)::out) is det,
	func scale(float, float, T) = T
].

%------------------------------------------------------------------------------%

:- func scale(float, T) = T <= shape(T).

%------------------------------------------------------------------------------%

:- pred triangle_intersects_segments(triangle, list(segment), point).
:- mode triangle_intersects_segments(in, in, out) is semidet.

%------------------------------------------------------------------------------%

:- instance shape(segment).
:- instance shape(rectangle).
:- instance shape(circle).
:- instance shape(point).
:- instance shape(triangle).

%==============================================================================%
:- implementation.
%==============================================================================%

:- use_module math.
:- import_module float.

:- use_module mmath.geometry.
:- use_module mmath.geometry.segment.
:- use_module mmath.multi_math.
:- import_module mmath.vector.vector2.

%------------------------------------------------------------------------------%

:- pragma inline(segment/4).
segment(X1, Y1, X2, Y2) = segment(vector(X1, Y1), vector(X2, Y2)).

seg_x1(segment(vector(X, _), _)) = X.
seg_y1(segment(vector(_, Y), _)) = Y.
seg_x2(segment(_, vector(X, _))) = X.
seg_y2(segment(_, vector(_, Y))) = Y.

'seg_x1 :='(X, segment(vector(_, Y), P2)) = segment(vector(X, Y), P2).
'seg_y1 :='(Y, segment(vector(X, _), P2)) = segment(vector(X, Y), P2).
'seg_x2 :='(X, segment(P1, vector(_, Y))) = segment(P1, vector(X, Y)).
'seg_y2 :='(Y, segment(P1, vector(X, _))) = segment(P1, vector(X, Y)).

%------------------------------------------------------------------------------%

:- pragma inline(rectangle/4).
rectangle(X, Y, W, H) = rectangle(vector(X, Y), W, H).

rect_x(rectangle(vector(X, _), _, _)) = X.
rect_y(rectangle(vector(_, Y), _, _)) = Y.

'rect_x :='(X, rectangle(vector(_, Y), W, H)) = rectangle(vector(X, Y), W, H).
'rect_y :='(Y, rectangle(vector(X, _), W, H)) = rectangle(vector(X, Y), W, H).

%------------------------------------------------------------------------------%

:- pragma inline(circle/3).
circle(X, Y, R) = circle(vector(X, Y), R).

circle_x(circle(vector(X, _), _)) = X.
circle_y(circle(vector(_, Y), _)) = Y.

'circle_x :='(X, circle(vector(_, Y), R)) = circle(vector(X, Y), R).
'circle_y :='(Y, circle(vector(X, _), R)) = circle(vector(X, Y), R).

%------------------------------------------------------------------------------%

:- pragma inline(point/2).
point(X, Y) = vector(X, Y).

x(vector(X, _)) = X.
y(vector(_, Y)) = Y.

'x :='(X, vector(_, Y)) = vector(X, Y).
'y :='(Y, vector(X, _)) = vector(X, Y).

%------------------------------------------------------------------------------%

:- pragma inline(triangle/6).
triangle(X1, Y1, X2, Y2, X3, Y3) = triangle(
    vector(X1, Y1),
    vector(X2, Y2),
    vector(X3, Y3)).

tri_x1(triangle(vector(X, _), _, _)) = X.
tri_y1(triangle(vector(_, Y), _, _)) = Y.
tri_x2(triangle(_, vector(X, _), _)) = X.
tri_y2(triangle(_, vector(_, Y), _)) = Y.
tri_x3(triangle(_, _, vector(X, _))) = X.
tri_y3(triangle(_, _, vector(_, Y))) = Y.

'tri_x1 :='(X, triangle(vector(_, Y), P2, P3)) = triangle(vector(X, Y), P2, P3).
'tri_y1 :='(Y, triangle(vector(X, _), P2, P3)) = triangle(vector(X, Y), P2, P3).
'tri_x2 :='(X, triangle(P1, vector(_, Y), P3)) = triangle(P1, vector(X, Y), P3).
'tri_y2 :='(Y, triangle(P1, vector(X, _), P3)) = triangle(P1, vector(X, Y), P3).
'tri_x3 :='(X, triangle(P1, P2, vector(_, Y))) = triangle(P1, P2, vector(X, Y)).
'tri_y3 :='(Y, triangle(P1, P2, vector(X, _))) = triangle(P1, P2, vector(X, Y)).

%------------------------------------------------------------------------------%

% is_in_range(X, M, N)
% Unifies if X is between M and N
:- pred is_in_range(float, float, float).
:- mode is_in_range(in, in, in) is semidet.

is_in_range(X, M, N) :- X >= M, X =< N.

%------------------------------------------------------------------------------%

%:- promise all[Segment, Intercept] (
%    intercept(Segment, Intercept) <=> float.is_finite(slope(Segment))
%).

%------------------------------------------------------------------------------%

%:- promise all[Segment, Intercept] (
%    intercept(Segment, Intercept) <=> not float.is_infinite(slope(Segment))
%).

%------------------------------------------------------------------------------%

length(segment(X1, Y1, X2, Y2)) = distance(X1, Y1, X2, Y2).

%------------------------------------------------------------------------------%

distance(point(X1, Y1), point(X2, Y2)) = distance(X1, Y1, X2, Y2).

%------------------------------------------------------------------------------%

distance(X1, Y1, X2, Y2) = math.sqrt((XDiff * XDiff) + (YDiff * YDiff)) :-
    XDiff = X1 - X2,
    YDiff = Y1 - Y2.

:- pragma inline(distance/4).

%------------------------------------------------------------------------------%

grow_rectangle(Size, In) = grow_rectangle(Size, Size, In).
grow_rectangle(Size, !Rectangle) :- grow_rectangle(Size, Size, !Rectangle).

%------------------------------------------------------------------------------%

grow_rectangle(DX, DY, In) = Out :- grow_rectangle(DX, DY, In, Out).
grow_rectangle(DX, DY,
    rectangle(X, Y, W, H),
    rectangle(X - DX, Y - DY, W + DX + DX, H + DY + DY)).

%------------------------------------------------------------------------------%

rectangle_intersection(rectangle(X1, Y1, W1, H1),
    rectangle(X2, Y2, W2, H2),
    rectangle(Left + 0.0, Top + 0.0, OutW, OutH)) :-
    
    mmath.multi_math.sub(Right, Left, OutW),
    mmath.multi_math.sub(Bottom, Top, OutH),
    
    % Logically required for an intersection to have occurred
    Bottom >= Top,
    Right >= Left,
    
    Left = max(X1, X2),
    Top = max(Y1, Y2),
    
    Right = min(X1 + W1, X2 + W2),
    Bottom = min(Y1 + H1, Y2 + H2).

%------------------------------------------------------------------------------%

rectangles_intersect(R1, R2) :- rectangle_intersection(R1, R2, _).

%------------------------------------------------------------------------------%

rectangle_union(rectangle(X1, Y1, W1, H1),
    rectangle(X2, Y2, W2, H2),
    rectangle(Left + 0.0, Top + 0.0, OutW, OutH)) :-
    
    mmath.multi_math.sub(Right, Left, OutW),
    mmath.multi_math.sub(Bottom, Top, OutH),
    
    Left = min(X1, X2),
    Top = min(Y1, Y2),
    
    Right = max(X1 + W1, X2 + W2),
    Bottom = max(Y1 + H1, Y2 + H2).

%------------------------------------------------------------------------------%

rectangle_union(R1, R2) = ROut :- rectangle_union(R1, R2, ROut).

%------------------------------------------------------------------------------%

rectangle_contains(rectangle(X1, Y1, W1, H1), rectangle(X2, Y2, W2, H2)) :-
    X1 >= X2,
    Y1 >= Y2,
    X1 + W1 =< X2 + W2,
    Y1 + H1 =< Y2 + H2.

%------------------------------------------------------------------------------%

triangle_segments(triangle(P1, P2, P3), S1, S2, S3) :-
    S1 = segment(P1, P2),
    S2 = segment(P2, P3),
    S3 = segment(P3, P1).

%------------------------------------------------------------------------------%

translate(Point, Shape) = Out :- translate(Point, Shape, Out).

%------------------------------------------------------------------------------%

slope(Segment, slope(Segment)).

slope(segment(X1, Y1, X2, Y2)) = (Slope) :-
    ( if
        X1 = X2
    then
        Slope = infinity
    else
        % Rise/Run, difference of Y over difference of X
        Slope = (Y2 - Y1) / (X2 - X1)
    ).

%------------------------------------------------------------------------------%

intercept(S, Intercept) :- intercept(S, Intercept, _).

intercept(S, Intercept, Slope + 0.0) :-
    slope(S, Slope),
    is_finite(Slope),
    % Finding the Y-intercept:
    % b = y-mx
    Intercept = S ^ seg_y1 - (S ^ seg_x1 * Slope).

%------------------------------------------------------------------------------%

solve_x(Segment, X, (Slope * X) + Intercept) :-
    % Y = mx + b
    intercept(Segment, Intercept, Slope).

%------------------------------------------------------------------------------%

collide_segments(S1, S2, Point) :-
    S1 = segment(S1X1, S1Y1, S1X2, S1Y2),
    S2 = segment(S2X1, S2Y1, S2X2, S2Y2),
    
    Box1 = grow_rectangle(float.epsilon, mmath.geometry2d.bounding_box(S1)),
    Box2 = grow_rectangle(float.epsilon, mmath.geometry2d.bounding_box(S2)),
    rectangle_intersection(Box1, Box2, IntersectedBox),
    grow_rectangle(float.epsilon, IntersectedBox, GrownIntersectedBox),
    point_inside(GrownIntersectedBox, Point),
    
    slope(S1, Slope1),
    slope(S2, Slope2),
    
    % Get the intersection point
    ( if
        % The simplest check we can do is for parallel lines
        (is_infinite(Slope1), is_infinite(Slope2)) ;
        (
            is_finite(Slope1),
            is_finite(Slope2),
            abs(Slope1 - Slope2) < epsilon
        )
    then
        false,
        % The slopes are both infinite, so both lines are vertical.
        % This unification ensures they have the same X.
        is_infinite(Slope1) => abs(S1X1 - S2X1) < epsilon,
        is_infinite(Slope2) => abs(S1X2 - S2X2) < epsilon,
        
        % If they are not vertical, then they should have the same intercept
        geometry2d.intercept(S1, Intercept1Test) => 
        (
            intercept(S2, Intercept2Test),
            abs(Intercept1Test - Intercept2Test) < epsilon
        ),
        
        Point = point(IntersectX, IntersectY),
        
        ( if
            is_finite(Slope1), intercept(S1, Intercept1)
        then
            % y = mx + b
            % y - b = mx
            % (y - b) / m = x
            IntersectX = (IntersectY - Intercept1) / Slope1
        else
            IntersectX = S1X1
        ),
        
        % Find a Y where both lines are present
        ( if % Check S1 Y1
            is_in_range(S1Y1, S2Y1, S2Y2)
        then
            IntersectY = S1Y1
        else if % Check S1 Y2
            is_in_range(S1Y2, S2Y1, S2Y2)
        then
            IntersectY = S1Y2
        else if % Check S2 Y1
            is_in_range(S2Y1, S1Y1, S1Y2)
        then
            IntersectY = S2Y1
        else
            % Force a unification on S2 Y2.
            % If this fails, then the segments are on the smae line but do not intersect.
            is_in_range(S2Y2, S1Y1, S1Y2),
            IntersectY = S2Y2
        )
    else if
        is_infinite(Slope1),
        intercept(S2, Intercept2)
    then
        Point = point(S1X1, (Slope2*S1X1) + Intercept2)
    else if
        is_infinite(Slope2),
        intercept(S1, Intercept1)
    then
        Point = point(S2X1, (Slope1*S2X1) + Intercept1)
    else
        intercept(S1, Intercept1),
        intercept(S2, Intercept2),
        % y = m x + b
        % ------------
        % m1 * x + b1 = m2 * x + b2
        % m1 * x = m2 * x + b2 - b1
        % m1 * x - m2 * x = b2 - b1
        % x * ( m1 - m2 ) = b2 - b1
        % x = ( b2 - b1 ) / ( m1 - m2 )
        IntersectX = (Intercept2 - Intercept1) / (Slope1 - Slope2),
        % y = m * x + b ; where x = `IntersectX'
        IntersectY = (Slope1 * IntersectX) + Intercept1,
        Point = geometry2d.point(IntersectX, IntersectY)
    ).

%------------------------------------------------------------------------------%

collide_segment_circle(segment(P1, P2), Circle, P) :-
    
    % If neither of the end points are within the circle, then check if the circle's
    % center is within the radius of the segment
    ( if
        point_inside(Circle, P1)
    then
        P = P1
    else if
        point_inside(Circle, P2)
    then
        P = P2
    else
        % If we take the dot product of the center of the circle and one of the
        % endpoints on the segment, this will tell us what the nearest point on
        % the segment is to the circle.
        % We can then test this location on the line for being within the radius
        % of the center of the circle.
        Circle = geometry2d.circle(Center, _),
        
        % Adjust the center and the segment to start at Point1
        SegmentVector = P2 - P1,
        CenterVector = Center - P1,
        
        % Get the length of the segment, which is our scaling value
        Length = vector.magnitude(SegmentVector),
        
        % Determine the dot/T value
        T = vector.dot(SegmentVector/Length, CenterVector/Length),
        
        T >= 0.0, T =< 1.0,
        
        SegmentVector * T = Nearest,
        P =  Nearest + P1,
        point_inside(Circle, P)
    ).

%------------------------------------------------------------------------------%

collide_segment_rectangle(Segment, Rect, P) :-
    Segment = segment(P1, P2), 
    
    % If neither of the end points are within the rectangle, then try to
    % intersect the segments.
    ( if
        point_inside(Rect, P1)
    then
        P = P1
    else if
        point_inside(Rect, P2)
    then
        P = P2
    else
        rectangles_intersect(Rect, bounding_box(Segment)),
        Rect = rectangle(X, Y, W, H),
        C1 = point(X,     Y),
        C2 = point(X + W, Y),
        C3 = point(X + W, Y + H),
        C4 = point(X,     Y + H),
        ( if
            collide_segments(segment(C1, C2), Segment, SemiP)
        then
            P = SemiP
        else if
            collide_segments(segment(C2, C3), Segment, SemiP)
        then
            P = SemiP
        else if
            collide_segments(segment(C3, C4), Segment, SemiP)
        then
            P = SemiP
        else
            collide_segments(segment(C4, C1), Segment, P)
        )
    ).

%------------------------------------------------------------------------------%

scale(Scale, T) = scale(Scale, Scale, T).

%------------------------------------------------------------------------------%

triangle_intersects_segments(Triangle, Segments, OutPoint) :-
    geometry2d.triangle_segments(Triangle, TS1, TS2, TS3),
    
    ( list.find_first_map(geometry2d.collide_segments(TS1), Segments, Point) ->
        OutPoint = Point
    ; list.find_first_map(geometry2d.collide_segments(TS2), Segments, Point) ->
        OutPoint = Point
    ;
      list.find_first_map(geometry2d.collide_segments(TS3), Segments, OutPoint)
    ).

%------------------------------------------------------------------------------%

point_to_vector(geometry2d.point(X, Y)) = vector(X, Y).

%------------------------------------------------------------------------------%

:- instance shape(segment) where [
    %(area(_) = 0.0),
    (center(segment(X1, Y1, X2, Y2)) =
        point((X1 + X2) * 0.5, (Y1 + Y2) * 0.5)),
    (bounding_box(segment(X1, Y1, X2, Y2)) = rectangle(X, Y, W, H) :-
        X = min(X1, X2),
        Y = min(Y1, Y2),
        W = max(X1, X2) - X,
        H = max(Y1, Y2) - Y),
    (point_inside(_, _) :- false),
    (translate(Translate, S1, S2) :-
        S1 = segment(S1P1, S1P2),
        S2 = segment(S2P1, S2P2),
        translate(Translate, S1P1, S2P1),
        translate(Translate, S1P2, S2P2)),
    (convex(segment(X1, Y1, X2, Y2), 0.0, [point(X1, Y1)|[point(X2, Y2)|[]]])),
	scale(X, Y, segment(X1, Y1, X2, Y2)) = segment(X1*X, Y1*Y, X2*X, Y2*Y)
].

%------------------------------------------------------------------------------%

:- instance shape(rectangle) where [
    %(area(Rect) = Rect ^ rect_w * Rect ^ rect_h),
    (center(rectangle(X, Y, W, H)) = point(X + (W / 2.0), Y + (H / 2.0))),
    (bounding_box(R) = R),
    (point_inside(rectangle(X, Y, W, H), point(PX, PY)) :-
        PX >= X, PY >= Y, PX =< X + W, PY =< Y + H),
    (translate(Translate,
        rectangle(XIn, YIn, W, H),
        rectangle(XOut, YOut, W, H)) :-
        translate(Translate, point(XIn, YIn), point(XOut, YOut))),
    (convex(rectangle(X, Y, W, H), 0.0, [
            point(X, Y) | [
            point(X + W, Y + H) | [
            point(X + W, Y + H) | [
            point(X, Y + H) | []]]]])),
	scale(ScaleX, ScaleY, rectangle(X, Y, W, H)) =
        rectangle(X*ScaleX, Y*ScaleY, W*ScaleX, H*ScaleY)
].

%------------------------------------------------------------------------------%

:- instance shape(circle) where [
    %(area(Circle) = math.pi * Circle ^ radius * Circle ^ radius),
    (center(circle(X, Y, _)) = point(X, Y)),
    (bounding_box(circle(X, Y, R)) = rectangle(X - R, Y - R, R + R, R + R)),
    (point_inside(circle(X, Y, R), point(PX, PY)) :-
        XDist = PX - X,
        YDist = PY - Y,
        (
            XDist = 0.0, YDist = 0.0
        ;
            math.sqrt((XDist * XDist) + (YDist * YDist)) < R - float.epsilon
        )
    ),
    (translate(Translate, circle(XIn, YIn, R), circle(XOut, YOut, R)) :-
        translate(Translate, point(XIn, YIn), point(XOut, YOut))),
    (convex(Circle, Circle ^ radius, [center(Circle)|[]])),
	scale(ScaleX, ScaleY, circle(X, Y, R)) =
        circle(X*ScaleX, Y*ScaleY, R*(ScaleX + ScaleY)*0.5) % TODO?
].

%------------------------------------------------------------------------------%

:- instance shape(point) where [
    %area(_) = 0.0,
    (center(P) = P),
    (bounding_box(point(X, Y)) = rectangle(X, Y, 0.0, 0.0)),
    (point_inside(P, P)),
    (translate(point(XT, YT), point(XIn, YIn), point(XOut, YOut)) :-
        mmath.multi_math.add(XT, XIn, XOut),
        mmath.multi_math.add(YT, YIn, YOut)),
    (convex(Point, 0.0, [Point|[]])),
	scale(ScaleX, ScaleY, point(X, Y)) = point(ScaleX*X, ScaleY*Y)
].

%------------------------------------------------------------------------------%

:- instance shape(triangle) where [
    %area(triangle) = 0.0,
    (center(triangle(X1, Y1, X2, Y2, X3, Y3)) =
        point((X1 + X2 + X3) / 3.0, (Y1 + Y2 + Y3) / 3.0)),
    (bounding_box(triangle(X1, Y1, X2, Y2, X3, Y3)) = rectangle(X, Y, W, H) :-
        X = min(min(X1, X2), X3),
        Y = min(min(Y1, Y2), Y3),
        W = max(max(X1, X2), X3) - X,
        H = max(max(Y1, Y2), Y3) - Y),
    (point_inside(triangle(X1, Y1, X2, Y2, X3, Y3), point(PX, PY)) :-
        % Find the side of the point for each segment
        Pos = vector(PX, PY),
        Vector1 = vector(X1, Y1),
        Vector2 = vector(X2, Y2),
        Vector3 = vector(X3, Y3),
        Segment1 = mmath.geometry.segment.segment(Vector1, Vector2),
        Segment2 = mmath.geometry.segment.segment(Vector2, Vector3),
        Segment3 = mmath.geometry.segment.segment(Vector3, Vector1),
        
        Side1 = mmath.geometry.segment.side(Segment1, Pos),
        Side2 = mmath.geometry.segment.side(Segment2, Pos),
        Side3 = mmath.geometry.segment.side(Segment3, Pos),
        
        % Either the point must be colinear, or we must be on the same side of
        % all three segments.
        (
            Side1 = mmath.geometry.segment.colinear
        ;
            Side2 = mmath.geometry.segment.colinear
        ;
            Side3 = mmath.geometry.segment.colinear
        ;
            Side1 = Side2,
            Side2 = Side3
        )
    ),
    (translate(P, triangle(!.P1, !.P2, !.P3), triangle(!:P1, !:P2, !:P3)) :-
        translate(P, !P1),
        translate(P, !P2),
        translate(P, !P3)
    ),
    convex(triangle(P1, P2, P3), 0.0, [P1|[P2|[P3|[]]]]),
    (scale(X, Y, triangle(P1, P2, P3)) = triangle(P * P1, P * P2, P * P3) :-
        P = vector(X, Y))
    
].
