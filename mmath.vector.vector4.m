% Copyright (C) 2017-2022 AlaskanEmily
%
% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at http://mozilla.org/MPL/2.0/.

:- module mmath.vector.vector4.

%==============================================================================%
% 4D vector implementation.
:- interface.
%==============================================================================%

:- type vector ---> vector(x::float, y::float, z::float, w::float).

%------------------------------------------------------------------------------%

:- instance vector(vector4).

%------------------------------------------------------------------------------%

:- func get_x(vector4) = float.
:- func get_y(vector4) = float.
:- func get_z(vector4) = float.
:- func get_w(vector4) = float.

%------------------------------------------------------------------------------%

:- func (vector4::in) + (vector4::in) = (vector4::uo) is det.
:- func (vector4::in) - (vector4::in) = (vector4::uo) is det.
:- func (vector4::in) * (vector4::in) = (vector4::uo) is det.
:- func (vector4::in) / (vector4::in) = (vector4::uo) is det.

%------------------------------------------------------------------------------%

:- pred foldl(pred(float, T, T), vector4, T, T).
:- mode foldl(pred(in, in, out) is det, in, in, out) is det.
:- mode foldl(pred(in, in, out) is semidet, in, in, out) is semidet.
:- mode foldl(pred(in, di, uo) is det, in, di, uo) is det.
:- mode foldl(pred(in, mdi, muo) is semidet, in, mdi, muo) is semidet.
:- mode foldl(pred(in, mdi, muo) is det, in, mdi, muo) is det.

%------------------------------------------------------------------------------%

:- pred foldr(pred(float, T, T), vector4, T, T).
:- mode foldr(pred(in, in, out) is det, in, in, out) is det.
:- mode foldr(pred(in, in, out) is semidet, in, in, out) is semidet.
:- mode foldr(pred(in, di, uo) is det, in, di, uo) is det.
:- mode foldr(pred(in, mdi, muo) is semidet, in, mdi, muo) is semidet.
:- mode foldr(pred(in, mdi, muo) is det, in, mdi, muo) is det.

%------------------------------------------------------------------------------%

:- pred foldl_field(pred(func(vector4)=(float), vector4, T, T), vector4, T, T).
:- mode foldl_field(pred(func(in)=(out) is det, in, in, out) is det, in, in, out) is det.
:- mode foldl_field(pred(func(in)=(out) is det, in, in, out) is semidet, in, in, out) is semidet.
:- mode foldl_field(pred(func(in)=(out) is det, in, mdi, muo) is semidet, in, mdi, muo) is semidet.
:- mode foldl_field(pred(func(in)=(out) is det, in, mdi, muo) is det, in, mdi, muo) is det.

%------------------------------------------------------------------------------%

:- pred foldr_field(pred(func(vector4)=(float), vector4, T, T), vector4, T, T).
:- mode foldr_field(pred(func(in)=(out) is det, in, in, out) is det, in, in, out) is det.
:- mode foldr_field(pred(func(in)=(out) is det, in, in, out) is semidet, in, in, out) is semidet.

%------------------------------------------------------------------------------%

:- pred map(pred(float, float), vector4, vector4).
:- mode map(pred(in, out) is det, in, out) is det.
:- mode map(pred(in, out) is semidet, in, out) is semidet.
:- mode map(pred(di, uo) is det, di, uo) is det.
:- mode map(pred(mdi, muo) is semidet, mdi, muo) is semidet.
:- mode map(pred(mdi, muo) is det, mdi, muo) is det.

%------------------------------------------------------------------------------%

:- pred map_field(pred(func(vector4)=(float), vector4, float), vector4, vector4).
:- mode map_field(pred(func(in)=(out) is det, in, out) is det, in, out) is det.
:- mode map_field(pred(func(in)=(out) is det, in, out) is semidet, in, out) is semidet.

%------------------------------------------------------------------------------%

:- pred map_foldl(pred(float, float, T, T), vector4, vector4, T, T).
:- mode map_foldl(pred(in, out, in, out) is det, in, out, in, out) is det.
:- mode map_foldl(pred(in, out, in, out) is semidet, in, out, in, out) is semidet.
:- mode map_foldl(pred(in, out, mdi, muo) is semidet, in, out, mdi, muo) is semidet.
:- mode map_foldl(pred(in, out, mdi, muo) is det, in, out, mdi, muo) is det.
:- mode map_foldl(pred(di, uo, in, out) is det, di, uo, in, out) is det.
:- mode map_foldl(pred(in, out, di, uo) is det, in, out, di, uo) is det.
:- mode map_foldl(pred(di, uo, di, uo) is det, di, uo, di, uo) is det.
:- mode map_foldl(pred(mdi, muo, in, out) is det, mdi, muo, in, out) is det.
:- mode map_foldl(pred(mdi, muo, in, out) is semidet, mdi, muo, in, out) is semidet.
:- mode map_foldl(pred(mdi, muo, mdi, muo) is semidet, mdi, muo, mdi, muo) is semidet.
:- mode map_foldl(pred(mdi, muo, mdi, muo) is det, mdi, muo, mdi, muo) is det.
:- mode map_foldl(pred(mdi, muo, di, uo) is det, mdi, muo, di, uo) is det.

%------------------------------------------------------------------------------%

:- pred map_foldr(pred(float, float, T, T), vector4, vector4, T, T).
:- mode map_foldr(pred(in, out, in, out) is det, in, out, in, out) is det.
:- mode map_foldr(pred(in, out, in, out) is semidet, in, out, in, out) is semidet.
:- mode map_foldr(pred(in, out, mdi, muo) is semidet, in, out, mdi, muo) is semidet.
:- mode map_foldr(pred(in, out, mdi, muo) is det, in, out, mdi, muo) is det.
:- mode map_foldr(pred(di, uo, in, out) is det, di, uo, in, out) is det.
:- mode map_foldr(pred(in, out, di, uo) is det, in, out, di, uo) is det.
:- mode map_foldr(pred(di, uo, di, uo) is det, di, uo, di, uo) is det.
:- mode map_foldr(pred(mdi, muo, in, out) is det, mdi, muo, in, out) is det.
:- mode map_foldr(pred(mdi, muo, in, out) is semidet, mdi, muo, in, out) is semidet.
:- mode map_foldr(pred(mdi, muo, mdi, muo) is semidet, mdi, muo, mdi, muo) is semidet.
:- mode map_foldr(pred(mdi, muo, mdi, muo) is det, mdi, muo, mdi, muo) is det.
:- mode map_foldr(pred(mdi, muo, di, uo) is det, mdi, muo, di, uo) is det.

%==============================================================================%
:- implementation.
%==============================================================================%

:- import_module float.

%------------------------------------------------------------------------------%

get_x(V) = V ^ x.
get_y(V) = V ^ y.
get_z(V) = V ^ z.
get_w(V) = V ^ w.

%------------------------------------------------------------------------------%

(vector(X1, Y1, Z1, W1)) + (vector(X2, Y2, Z2, W2)) = (vector(X1+X2, Y1+Y2, Z1+Z2, W1+W2)).
(vector(X1, Y1, Z1, W1)) - (vector(X2, Y2, Z2, W2)) = (vector(X1-X2, Y1-Y2, Z1-Z2, W1-W2)).
(vector(X1, Y1, Z1, W1)) * (vector(X2, Y2, Z2, W2)) = (vector(X1*X2, Y1*Y2, Z1*Z2, W1*W2)).
(vector(X1, Y1, Z1, W1)) / (vector(X2, Y2, Z2, W2)) = (vector(X1/X2, Y1/Y2, Z1/Z2, W1/W2)).

%------------------------------------------------------------------------------%

:- instance vector(vector4) where [
    magnitude_squared(vector(X, Y, Z, W)) = ((X*X) + (Y*Y) + (Z*Z) + (W*W)),
    scale(vector(X, Y, Z, W), S) = vector(X*S, Y*S, Z*S, W*S),
    multiply(vector(X1, Y1, Z1, W1), vector(X2, Y2, Z2, W2)) = vector(X1*X2, Y1*Y2, Z1*Z2, W1*W2),
    divide(vector(X1, Y1, Z1, W1), vector(X2, Y2, Z2, W2)) = vector(X1/X2, Y1/Y2, Z1/Z2, W1/W2),
    add(vector(X1, Y1, Z1, W1), vector(X2, Y2, Z2, W2)) = vector(X1+X2, Y1+Y2, Z1+Z2, W1+W2),
    sub(vector(X1, Y1, Z1, W1), vector(X2, Y2, Z2, W2)) = vector(X1-X2, Y1-Y2, Z1-Z2, W1-W2),
    dot(vector(X1, Y1, Z1, W1), vector(X2, Y2, Z2, W2)) = ((X1*X2) + (Y1*Y2) + (Z1*Z2) + (W1*W2)),
    reciprocal(vector(X, Y, Z, W)) = vector(1.0/X, 1.0/Y, 1.0/Z, 1.0/W)
].

%------------------------------------------------------------------------------%

foldl(Pred, vector(X, Y, Z, W), !T) :- Pred(X, !T), Pred(Y, !T), Pred(Z, !T), Pred(W, !T).

%------------------------------------------------------------------------------%

foldr(Pred, vector(X, Y, Z, W), !T) :- Pred(W, !T), Pred(Z, !T), Pred(Y, !T), Pred(X, !T).

%------------------------------------------------------------------------------%

foldl_field(Pred, Vec, !T) :-
    Pred(get_x, Vec, !T), Pred(get_y, Vec, !T), Pred(get_z, Vec, !T), Pred(get_w, Vec, !T).

%------------------------------------------------------------------------------%

foldr_field(Pred, Vec, !T) :-
    Pred(get_w, Vec, !T), Pred(get_z, Vec, !T), Pred(get_y, Vec, !T), Pred(get_x, Vec, !T).

%------------------------------------------------------------------------------%

map(Pred, vector(XIn, YIn, ZIn, WIn), vector(XOut, YOut, ZOut, WOut)) :-
    Pred(XIn, XOut),
    Pred(YIn, YOut),
    Pred(ZIn, ZOut),
    Pred(WIn, WOut).

%------------------------------------------------------------------------------%

map_field(Pred, Vec, vector(X, Y, Z, W)) :-
    Pred(get_x, Vec, X),
    Pred(get_y, Vec, Y),
    Pred(get_z, Vec, Z),
    Pred(get_w, Vec, W).

%------------------------------------------------------------------------------%

map_foldl(Pred, vector(XIn, YIn, ZIn, WIn), vector(XOut, YOut, ZOut, WOut), !T) :-
    Pred(XIn, XOut, !T),
    Pred(YIn, YOut, !T),
    Pred(ZIn, ZOut, !T),
    Pred(WIn, WOut, !T).

%------------------------------------------------------------------------------%

map_foldr(Pred, vector(XIn, YIn, ZIn, WIn), vector(XOut, YOut, ZOut, WOut), !T) :-
    Pred(WIn, WOut, !T),
    Pred(ZIn, ZOut, !T),
    Pred(YIn, YOut, !T),
    Pred(XIn, XOut, !T).

%------------------------------------------------------------------------------%
