% Copyright (C) 2017-2020 AlaskanEmily
%
% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at http://mozilla.org/MPL/2.0/.

:- module mmath.geometry.

%==============================================================================%
:- interface.
%==============================================================================%

:- import_module mmath.vector.

%------------------------------------------------------------------------------%

:- include_module mmath.geometry.triangle.
:- include_module mmath.geometry.segment.
:- include_module mmath.geometry.plane.

%------------------------------------------------------------------------------%

:- import_module mmath.geometry.triangle.
:- import_module mmath.geometry.segment.
:- import_module mmath.geometry.plane.

%------------------------------------------------------------------------------%

:- pred triangle_segments(triangle(T),
    segment.segment(T), segment.segment(T), segment.segment(T)).
:- mode triangle_segments(in, out, out, out) is det.
% Enforces winding order and origin
:- mode triangle_segments(out, in, in, in) is semidet.
:- mode triangle_segments(out, out, in, in) is semidet.
:- mode triangle_segments(out, in, out, in) is semidet.
:- mode triangle_segments(out, in, in, out) is semidet.

%------------------------------------------------------------------------------%

:- pred intersect(plane::in, segment(vector3)::in, float::out, vector3::out) is semidet.

%------------------------------------------------------------------------------%

:- pred intersect(segment(vector3), triangle(vector3), float, float, float, vector3).
:- mode intersect(in, in, out, out, out, out) is semidet.

%------------------------------------------------------------------------------%

:- pred intersect(triangle(vector3)::in, triangle(vector3)::in) is semidet.

%------------------------------------------------------------------------------%

:- pragma type_spec(triangle_segments/4, T = vector.vector2).
:- pragma type_spec(triangle_segments/4, T = vector.vector3).

%==============================================================================%
:- implementation.
%==============================================================================%

:- import_module float.

:- import_module mmath.vector.vector3.

%------------------------------------------------------------------------------%

triangle_segments(triangle(P1, P2, P3),
    segment(P1, P2), segment(P2, P3), segment(P3, P1)).

%------------------------------------------------------------------------------%

intersect(Plane, segment(P1, P2), T, Out) :-
    Plane = plane(Normal, D),
    plane.face(Plane, P1) = Face1,
    plane.face(Plane, P2) = Face2,
    % Check for coplanar points, and that the line even traverses the plane at all.
    ( if 
        Face1 = plane.coplanar
    then
        T = 0.0,
        Out = P1
    else if
        Face2 = plane.coplanar
    then
        T = 1.0,
        Out = P2
    else
        ( not Face1 = Face2 ),
        
        require_det (
            P1NormLength = dot(Normal, P1),
            P2NormLength = dot(Normal, P2),
            ( if
                P2NormLength > P1NormLength
            then
                LenNorm = P2NormLength - P1NormLength,
                S = (D - P1NormLength) / LenNorm
            else
                LenNorm = P1NormLength - P2NormLength,
                S = 1.0 - ((D - P2NormLength) / LenNorm)
            )
        ),
        
        % This indicates a collinear segment. We could technically choose any
        % point on the line...and maybe we should?
        not float.is_zero(S),
        
        require_det (
            CalcT = D / S,
            % TODO: We already determined the line traverses the plane?
            % Snap results that are an epsilon away at the edges of 0-1
            ( CalcT >= -float.epsilon, CalcT =< 0.0      -> T = 0.0
            ; CalcT =< 1.0 + float.epsilon, CalcT >= 1.0 -> T = 1.0
            ; T = CalcT ),
            Out = solve(segment(P1, P2), T)
        )
    ).

%------------------------------------------------------------------------------%

intersect(S, Tri, Out1, Out2, Out3, P) :-
    triangle.normal(Tri) = Normal,
    D = triangle.plane_d(Tri, Normal),
    intersect(plane(Normal, D), S, _T, P),
    triangle.collinear(Tri, P, Out1, Out2, Out3).

%------------------------------------------------------------------------------%

:- pred triangle_intersects_segments(triangle(vector3)::in,
    segment(vector3)::in,
    segment(vector3)::in,
    segment(vector3)::in) is semidet.

triangle_intersects_segments(Tri, S1, S2, S3) :-
    intersect(S1, Tri, _, _, _, _) ;
    intersect(S2, Tri, _, _, _, _) ;
    intersect(S3, Tri, _, _, _, _).

%------------------------------------------------------------------------------%

intersect(T1, T2) :-
    triangle_segments(T1, T1S1, T1S2, T1S3),
    triangle_segments(T2, T2S1, T2S2, T2S3),
    (
        triangle_intersects_segments(T2, T1S1, T1S2, T1S3)
    ;
        triangle_intersects_segments(T1, T2S1, T2S2, T2S3)
    ).
