% Copyright (C) 2017-2020 AlaskanEmily
%
% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at http://mozilla.org/MPL/2.0/.

:- module mmath.geometry.plane.

%==============================================================================%
% Geometric operations related to a 3D plane.
:- interface.
%==============================================================================%

:- import_module mmath.vector.

%------------------------------------------------------------------------------%

:- type plane ---> plane(normal::vector3, d::float).

%------------------------------------------------------------------------------%

:- type face ---> front ; back ; coplanar.

%------------------------------------------------------------------------------%

:- func a(plane) = float.
:- func b(plane) = float.
:- func c(plane) = float.

%------------------------------------------------------------------------------%

:- pred as_vector4(plane, vector4).
:- mode as_vector4(in, out) is det.
:- mode as_vector4(out, in) is det.
:- mode as_vector4(in, in) is semidet.

%------------------------------------------------------------------------------%

:- func face(plane, vector3) = face.

%------------------------------------------------------------------------------%

:- pred as_tuple(plane, {float, float, float, float}).
:- mode as_tuple(in, out) is det.
:- mode as_tuple(out, in) is det.
:- mode as_tuple(in, in) is semidet.

%------------------------------------------------------------------------------%

:- func z_plane = plane.

%==============================================================================%
:- implementation.
%==============================================================================%

:- use_module math.

:- import_module mmath.vector.vector3.
:- import_module mmath.vector.vector4.

%------------------------------------------------------------------------------%

a(P) = P ^ normal ^ x.
b(P) = P ^ normal ^ y.
c(P) = P ^ normal ^ z.

%------------------------------------------------------------------------------%

face(P, V) = Face :-
    dot(P ^ normal, V) = T,
    builtin.compare(Result, T, P ^ d),
    (
        Result = (=),
        Face = coplanar
    ;
        Result = (<),
        Face = back
    ;
        Result = (>),
        Face = front
    ).

%------------------------------------------------------------------------------%

as_vector4(plane(vector(X, Y, Z), D), vector(X, Y, Z, D)).

%------------------------------------------------------------------------------%

as_tuple(plane(vector(X, Y, Z), D), {X, Y, Z, D}).

%------------------------------------------------------------------------------%

z_plane = plane(vector(0.0, 0.0, 1.0), 0.0).
