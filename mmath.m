% Copyright (C) 2017-2020 AlaskanEmily
%
% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at http://mozilla.org/MPL/2.0/.

:- module mmath.

%==============================================================================%
% This module simply pulls in all the modules of mmath in order to create a
% single library from them.
:- interface.
%==============================================================================%

:- include_module mmath.vector.
:- include_module mmath.geometry.
:- include_module mmath.matrix.
:- include_module mmath.geometry2d.
:- include_module mmath.multi_math.

