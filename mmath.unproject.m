% Copyright (C) 2019-2020 AlaskanEmily
%
% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at http://mozilla.org/MPL/2.0/.

:- module unproject.

%==============================================================================%
% Unproject a poition using a matrix.
:- interface.
%==============================================================================%

:- use_module matrix.
:- use_module vector.

%------------------------------------------------------------------------------%
% unproject(Screen, CameraMatrix, P1, P2).
% Outputs two points which are on a ray cast straight out of the screen which
% line on the normalized screen coordinate given.
:- pred unproject(vector.vector2, matrix.matrix, vector.vector3, vector.vector3).
:- mode unproject(in, in, uo, uo) is det.

%==============================================================================%
:- implementation.
%==============================================================================%

:- import_module float.

:- import_module matrix.invert.
:- import_module vector.vector3.
:- import_module vector.vector4.

%------------------------------------------------------------------------------%

:- func div_by_w(vector.vector4::in) = (vector.vector3::uo) is det.
div_by_w(vector(X, Y, Z, W)) = vector(X/W, Y/W, Z/W).

:- pragma inline(div_by_w/1).

%------------------------------------------------------------------------------%

unproject(Screen2DNormalized, CameraMatrix, div_by_w(P14D), div_by_w(P24D)) :-
    InverseCameraMatrix = invert(CameraMatrix),
    
    % Create the two points on the line
    Selected1Camera = vector.vector2_to_vector4(Screen2DNormalized,-1.0, 1.0),
    Selected2Camera = vector.vector2_to_vector4(Screen2DNormalized, 1.0, 1.0),
    
    % Project by the inverse matrix (unproject by the camera matrix)
    matrix.transform(InverseCameraMatrix, Selected1Camera) = P14D,
    matrix.transform(InverseCameraMatrix, Selected2Camera) = P24D.