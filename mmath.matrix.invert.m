% Copyright (C) 2017-2022 AlaskanEmily
%
% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at http://mozilla.org/MPL/2.0/.

:- module mmath.matrix.invert.

%==============================================================================%
% Inversion operations on matrices.
% This is a submodule because it contains many functions only useful for
% inverting matrices.
:- interface.
%==============================================================================%

%------------------------------------------------------------------------------%
% Inverts a matrix in one function.
:- func invert(matrix::in) = (matrix::uo) is det.

%==============================================================================%
:- implementation.
%==============================================================================%

:- import_module mmath.vector.
:- import_module mmath.vector.vector4.

% Input
%  0-A  1-B  2-C  3-D
%  4-E  5-F  6-G  7-H
%  8-I  9-J 10-K 11-L
% 12-M 13-N 14-O 15-P

% Transposed
%  0-A  1-E  2-I  3-M
%  4-B  5-F  6-J  7-N
%  8-C  9-G 10-K 11-O
% 12-D 13-H 14-L 15-P
    
invert(In) = matrix(OutA * Det, OutB * Det, OutC * Det, OutD * Det) :-
    
    % Compute the determinant.
    Det = (1.0 / dot(column_a(In), OutA)),
    
    % Calculate the cofactors for the first two rows
    A = (In ^ c ^ z) * (In ^ d ^ w), % 0
    B = (In ^ d ^ z) * (In ^ c ^ w), % 1
    C = (In ^ b ^ z) * (In ^ d ^ w), % 2
    D = (In ^ d ^ z) * (In ^ b ^ w), % 3
    E = (In ^ b ^ z) * (In ^ c ^ w), % 4
    F = (In ^ c ^ z) * (In ^ b ^ w), % 5
    G = (In ^ a ^ z) * (In ^ d ^ w), % 6
    H = (In ^ d ^ z) * (In ^ a ^ w), % 7
    I = (In ^ a ^ z) * (In ^ c ^ w), % 8
    J = (In ^ c ^ z) * (In ^ a ^ w), % 9
    K = (In ^ a ^ z) * (In ^ b ^ w), % 10
    L = (In ^ b ^ z) * (In ^ a ^ w), % 11
    
    % Calculate the first two rows using the cofactors.
    OutA = mmath.vector.vector4.vector(
        (A * In ^ b ^ y) + (D * In ^ c ^ y) + (E * In ^ d ^ y)
      -((B * In ^ b ^ y) + (C * In ^ c ^ y) + (F * In ^ d ^ y)),
        (B * In ^ a ^ y) + (G * In ^ c ^ y) + (J * In ^ d ^ y)
      -((A * In ^ a ^ y) + (H * In ^ c ^ y) + (I * In ^ d ^ y)),
        (C * In ^ a ^ y) + (H * In ^ b ^ y) + (K * In ^ d ^ y)
      -((D * In ^ a ^ y) + (G * In ^ b ^ y) + (L * In ^ d ^ y)),
        (F * In ^ a ^ y) + (I * In ^ b ^ y) + (L * In ^ c ^ y)
      -((E * In ^ a ^ y) + (J * In ^ b ^ y) + (K * In ^ c ^ y))),
    
    OutB = mmath.vector.vector4.vector(
        (B * In ^ b ^ x) + (C * In ^ c ^ x) + (F * In ^ d ^ x)
      -((A * In ^ b ^ x) + (D * In ^ c ^ x) + (E * In ^ d ^ x)),
        (A * In ^ a ^ x) + (H * In ^ c ^ x) + (I * In ^ d ^ x)
      -((B * In ^ a ^ x) + (G * In ^ c ^ x) + (J * In ^ d ^ x)),
        (D * In ^ a ^ x) + (G * In ^ b ^ x) + (L * In ^ d ^ x)
      -((C * In ^ a ^ x) + (H * In ^ b ^ x) + (K * In ^ d ^ x)),
        (E * In ^ a ^ x) + (J * In ^ b ^ x) + (K * In ^ c ^ x)
      -((F * In ^ a ^ x) + (I * In ^ b ^ x) + (L * In ^ c ^ x))),

    % Calculate the cofactors for the last two rows
    M = (In ^ c ^ x) * (In ^ d ^ y), % 0
    N = (In ^ d ^ x) * (In ^ c ^ y), % 1
    O = (In ^ b ^ x) * (In ^ d ^ y), % 2
    P = (In ^ d ^ x) * (In ^ b ^ y), % 3
    Q = (In ^ b ^ x) * (In ^ c ^ y), % 4
    R = (In ^ c ^ x) * (In ^ b ^ y), % 5
    S = (In ^ a ^ x) * (In ^ d ^ y), % 6
    T = (In ^ d ^ x) * (In ^ a ^ y), % 7
    U = (In ^ a ^ x) * (In ^ c ^ y), % 8
    V = (In ^ c ^ x) * (In ^ a ^ y), % 9
    W = (In ^ a ^ x) * (In ^ b ^ y), % 10
    X = (In ^ b ^ x) * (In ^ a ^ y), % 11
    
    OutC = mmath.vector.vector4.vector(
        (M * In ^ b ^ w) + (P * In ^ c ^ w) + (Q * In ^ d ^ w)
      -((N * In ^ b ^ w) + (O * In ^ c ^ w) + (R * In ^ d ^ w)),
        (N * In ^ a ^ w) + (S * In ^ c ^ w) + (V * In ^ d ^ w)
      -((M * In ^ a ^ w) + (T * In ^ c ^ w) + (U * In ^ d ^ w)),
        (O * In ^ a ^ w) + (T * In ^ b ^ w) + (W * In ^ d ^ w)
      -((P * In ^ a ^ w) + (S * In ^ b ^ w) + (X * In ^ d ^ w)),
        (R * In ^ a ^ w) + (U * In ^ b ^ w) + (X * In ^ c ^ w)
      -((Q * In ^ a ^ w) + (V * In ^ b ^ w) + (W * In ^ c ^ w))),
    
    OutD = mmath.vector.vector4.vector(
        (N * In ^ b ^ z) + (O * In ^ c ^ z) + (R * In ^ d ^ z)
      -((M * In ^ b ^ z) + (P * In ^ c ^ z) + (Q * In ^ d ^ z)),
        (M * In ^ a ^ z) + (T * In ^ c ^ z) + (U * In ^ d ^ z)
      -((N * In ^ a ^ z) + (S * In ^ c ^ z) + (V * In ^ d ^ z)),
        (P * In ^ a ^ z) + (S * In ^ b ^ z) + (X * In ^ d ^ z)
      -((O * In ^ a ^ z) + (T * In ^ b ^ z) + (W * In ^ d ^ z)),
        (Q * In ^ a ^ z) + (V * In ^ b ^ z) + (W * In ^ c ^ z)
      -((R * In ^ a ^ z) + (U * In ^ b ^ z) + (X * In ^ c ^ z))).
